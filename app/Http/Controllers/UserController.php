<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Folder;
use App\Video;
use ZipArchive;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    const MAX_USER = 10;

    public function userManagement() 
    {
        return view('adminite.user_management');
    }

    public function showUser() 
    {
        $users = User::orderBy('id')->paginate(self::MAX_USER);

        return view('adminite.user_management', compact('users'));
    }

    public function createUser() 
    {
        return view('adminite.user.create');
    }

    public function storeUser(Request $request) 
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
            'email' => 'required|email|unique:users'
        ]);

        if (!empty($request->password)) {
            $password = trim($request->password);
        } else {
            $password = 'password';
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($password);
        $user->save();

        return redirect()->route('showUser');
    }

    public function editUser($id) 
    {
        $user = User::find($id);
        
        return view('adminite.user.edit', compact('user'));
    }

    public function updateUser(Request $request, $id) 
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->back();
    }

    public function deleteUser($id) 
    {
        $user = User::find($id);
        $user->delete();

        return response()->json(['data' => $user]);
    }
}
