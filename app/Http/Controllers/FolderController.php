<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Folder;
use App\Video;
use ZipArchive;
use Illuminate\Support\Facades\File;

class FolderController extends Controller
{
    const MAX_FOLDER = 10;

    public function showFolder() 
    {
        $folders = Folder::orderBy('id', 'desc')->paginate(self::MAX_FOLDER);

        return view('adminite.file.folder_management', compact('folders'));
    }

    public function createFolder() 
    {
        return view('adminite.file.folder_create');
    }

    public function storeFolder(Request $request) 
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
        ]);

        $folder = new Folder;
        $folder->name = $request->name;
        $folder->user_id = Auth::user()->id;
        $folder->email = Auth::user()->email;
        $folder->user_name = Auth::user()->name;
        $folderName = $folder->name;

        Storage::disk('public')->makeDirectory($folderName);

        $folder->save();
        
        return redirect()->route('fileManagementShowFolder');
    }

    public function editFolder($id) 
    {
        $folder = Folder::find($id);
        return view('adminite.file.folder_edit', compact('folder'));
    }

    public function updateFolder(Request $request, $id) 
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
        ]);

        $folder = Folder::find($id);
        $folder->name = $request->name;
        
        $folder->save();

        return redirect()->back();
    }

    public function deleteFolder($id) 
    {
        $folder = Folder::find($id);
        $folderName = $folder->name;
        Storage::disk('public')->deleteDirectory($folderName);

        $folder->delete();

        return response()->json(['data' => $folder]);
    }

    
    
}
