<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Input;
use App\Folder;
use App\Video;
use App\Comment;
use App\Rating;
use App\MailBox;
use ZipArchive;
use Illuminate\Support\Facades\File;
use FFMpeg;
use Tinify;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Jobs\SendWelcomeEmail;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEmail;
use Exception;
use Illuminate\Support\Facades\Log;

class MailController extends Controller
{
    const MAX_MAIL = 10;

    

    public function createMail() 
    {   
        $mails = DB::table('users')->get();

        return view('adminite.mail.mail_create', compact('mails'));
    }


    public function deleteMail($id) 
    {
        $mails = MailBox::find($id);

        $mails->delete();

        return redirect()->route('showMailBox', compact('mails'));
    }

    public function showContentMail($id) 
    {
        $mails = MailBox::find($id);
        return view('adminite.mail.mail_content', compact('mails', 'id'));
    }

    // /**
    //  * Handle Queue Process
    //  */
    // public function processQueue()
    // {
    //     // return view('adminite.mail.mail_test');
    //     return redirect()->route('createMail');
    // }

    public function showMailBox() 
    {
        $mails = MailBox::where('user_email', Auth::user()->email)
                                        ->orderBy('id', 'desc')
                                        ->paginate(self::MAX_MAIL);
        
        return view('adminite.mail.mail_box', compact('mails'));
    }

    public function storeMail(Request $request) 
    {
        $userName = $request->get('userName');
        foreach ($userName as $value) {
            $mails = new MailBox;

            $mails->title = $request->title;
            $mails->content = $request->content;
            $mails->user_id = 0;
            $mails->user_email = $request->get('user_email');
            $mails->user_email = $value;
            $mails->save();
        }

        $queues=[
            "userEmails" => $request->get('userName'),
            "title" => $request->title,
            "content" => $request->content,
        ]; 
        dispatch(new SendWelcomeEmail($queues));
        
        return redirect()->route('createMail');
    }
}
