<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Input;
use App\Folder;
use App\Video;
use App\Comment;
use App\Rating;
use ZipArchive;
use Illuminate\Support\Facades\File;
use FFMpeg;
use Tinify;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Jobs\SendWelcomeEmail;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEmail;
use Bloodhound;
use Tinify\Source;

class FileController extends Controller
{
    const MAX_VIDEO = 30;
    const MAX_COMMENT = 10;

    public function showVideo($id) 
    {
        if(Auth::user()->permission == "admin") {
            $videos = DB::table('videos')->where('folder_id', [(int) $id])
                                         ->leftjoin('ratings', 'videos.id', '=', 'ratings.video_id')
                                         ->select('videos.id', 'videos.thumbnail_name', 'status', 'videos.name', 'videos.user_name', 'videos.created_at', DB::raw('AVG(ratings.rating) as avg_rating'))
                                         ->groupBy('ratings.video_id', 'videos.id', 'videos.thumbnail_name', 'status', 'videos.name', 'videos.user_name', 'videos.created_at')
                                         ->orderBy('videos.id', 'desc')
                                         ->paginate(self::MAX_VIDEO); 
            
            $folders = Folder::whereNotIn('id', [(int) $id])->select('id', 'name')->get();  
            $thumbnailFolderName = Folder::find($id);
        } else {
            $videos = DB::table('videos')->where('videos.user_id', Auth::user()->id)
                                         ->where('folder_id', [(int) $id])
                                         ->orWhere('status', 'published')
                                         ->where('folder_id', [(int) $id])
                                         ->leftjoin('ratings', 'videos.id', '=', 'ratings.video_id')
                                         ->select('videos.id', 'videos.name', 'videos.thumbnail_name', 'videos.user_name', 'videos.created_at', DB::raw('AVG(ratings.rating) as avg_rating'))
                                         ->groupBy('ratings.video_id', 'videos.id', 'videos.name', 'videos.thumbnail_name', 'videos.user_name', 'videos.created_at')
                                         ->orderBy('avg_rating', 'desc')
                                         ->paginate(self::MAX_VIDEO);  
            $folders = Folder::whereNotIn('id', [(int) $id])->select('id', 'name')->get();
            $thumbnailFolderName = Folder::find($id);
        }

        return view('adminite.file.video_management', compact('videos','folders', 'thumbnailFolderName', 'id', 'id', 'id', 'id'));
    }

    public function storeVideo(Request $request, $id) 
    {
        $video = $request->file('video');

        $folderInfo = Folder::find($id);
        $name = $video->getClientOriginalName();
        // $destinationPath = public_path('/video/'.$folderInfo->name);
        // $video->move($destinationPath, $name);
        Storage::disk('public')->put($folderInfo->name.'/'.$name, file_get_contents($video));
        
        // $ffmpeg = "/usr/bin/ffmpeg";
        $ffmpeg = "/app/vendor/ffmpeg/ffmpeg";
        $thumbnailName = explode('.',$name);
        $thumbnailImage = "$thumbnailName[0].png";
        $size = "130x100";
        $cmd = "$ffmpeg -i $video -an -s $size $thumbnailImage";
        !shell_exec($cmd);

        // if( !shell_exec($cmd) ) {
            // $optimizedThumbnail = \Tinify\setKey("h47KGHWkmjcD1Ck3FNCtyTt680DM1D5p");
            // $optimizedThumbnail = \Tinify\fromFile($thumbnailImage);
            // $optimizedThumbnail->toFile($thumbnailImage);

            // Storage::disk('public')->put($thumbnailImage, file_get_contents($video));
            
            // $oldDestinationPath = public_path('/video/'.$thumbnailImage);
            // $newDestinationPath = public_path($thumbnailImage);
            // File::move($oldDestinationPath, $newDestinationPath);
        // }

        $videoFolder = new Video;
        $videoFolder->name = $name;
        $videoFolder->user_id = Auth::user()->id;
        $videoFolder->email = Auth::user()->email;
        $videoFolder->user_name = Auth::user()->name;
        $videoFolder->folder_id = $id;
        $videoFolder->thumbnail_name = $thumbnailImage;
        
        $videoFolder->save();

        return response()->json(['data' => $videoFolder]);
    }

    public function viewVideoManagement($id) 
    {
        $video = Video::find($id);
        $folderInfo = Folder::where('id', $video->folder_id)->first();
        
        $comments = Comment::orderBy('id');
        $comments = DB::table('comments')->where('video_id', (int) $id)->paginate(self::MAX_COMMENT);
        
        return view('adminite.file.view_management', compact('video', 'folderInfo', 'comments', 'id', 'id', 'id'));
    }

    public function updateStatusVideo($id) 
    {
        $video = Video::find($id);
        // $video = Video::where('folder_id', $id)->first();
        $video->status = 'published';

        $video->save();
        
        return response()->json(['data' => $video]);
    }

    public function deleteVideo($id) 
    {
        $video = Video::find($id);
        // $folderInfo = Folder::where('id', (int) $video->folder_id)->first();
        // $videoName = $video->name;
        // $thumbnailName = $video->thumbnail_name;
        // $path = public_path('/video/'.$folderInfo->name.'/'.$videoName);
        // $thumbnailPath = public_path('video/'.$thumbnailName);

        // unlink($path);
        // unlink($thumbnailPath);

        $video->delete();

        return response()->json(['data' => $video]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadZipVideo($id) 
    {
        $video = Video::find($id);
        $folderInfo = Folder::where('id', (int) $video->folder_id)->first();
        $publicDirectory = public_path('/video/'.$folderInfo->name);
        $zipFileName = "$video->name.zip";

        $zip = new ZipArchive;
        if ($zip->open($publicDirectory . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            $zip->addFile(public_path('/video/'.$folderInfo->name.'/'.$video->name),$video->name);   
            $zip->close();
        }
        
        $filetopath = $publicDirectory.'/'.$zipFileName;
        if ( file_exists($filetopath) ) {
            return response()->download($filetopath, $zipFileName);
        }
    }

    public function storeRating(Request $request, $id) 
    {
        $video = new Rating;
        $video->user_id = Auth::user()->id;
        $video->email = Auth::user()->email;
        $video->video_id = $id;
        $video->rating = $request->rate;

        $video->save();
        $video->rating = intval($video->rating);
        
        return response()->json(['data' => $video]);
    }

    public function storeComment(Request $request, $id) 
    {
        $this->validate($request, [
            'content'  => 'required|max:255',
        ]);
        
        $comment = new Comment;
        $comment->content = $request->content;
        $comment->user_id = Auth::user()->id;
        $comment->email = Auth::user()->email;
        $comment->video_id = $id;

        $comment->save();

        return response()->json(['data' => $comment]);
    }

    public function moveVideo(Request $request, $id) 
    {
        $video = Video::where('folder_id', $id)->first();
        // $video = Video::find($id);
        $currentFolder = Folder::where('id', (int) $id)->first();
        // $popupSelectFolder = $request->popupChooseFolder;
        $fowardFolder = Folder::where('id', (int) $request->popupChooseFolder)->first();
        // $video->folder_id = $fowardFolder->id;
        // $video->save();

        $video->folder_id = $request->popupChooseFolder;
        $video->save();

        File::move(public_path('/video/'.$currentFolder->name.'/'.$video->name), public_path('/video/'.$fowardFolder->name.'/'.$video->name));

        return response()->json(['data' => $video]);
    }

    public function searchByName(Request $request)
    {
        return Video::where('name', 'LIKE', '%'.$request->value.'%')->get();
    }

    // public function getUserIp(Request $request)
    // {  
    //     $geoip = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
    //     dd($geoip->country);  

    //     $ip = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
    //     $ip = $request->getClientIp();
    //     dd("Public IP: ".$ip);
    // }  

    public function tryAgain()
    {  
        return view('adminite.try_again');
    }  

    // public function searchUsers(Request $request)
    // {
    // return User::where('name', 'LIKE', '%'.$request->q.'%')->get();
    
    // }

}
