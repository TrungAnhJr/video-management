<?php

namespace App\Http\Middleware;

use Closure;

class CheckIP
{
    public $geoip;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
        $geoip = geoip()->getLocation($ip);
        if($geoip->country == 'Vietnam') {
            return $next($request);
        } else  {
            return redirect()->route('tryAgain');
        }
        
    }
}
