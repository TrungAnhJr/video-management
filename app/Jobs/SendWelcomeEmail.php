<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEmail;
use Illuminate\Http\Request;

class SendWelcomeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $queues;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($queues)
    {
        $this->queues = $queues;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {;
        foreach($this->queues['userEmails'] as $value) {
            unset($this->queues['userEmails']);
            Mail::to($value)->send(new WelcomeEmail($this->queues));
        }
    }
}
