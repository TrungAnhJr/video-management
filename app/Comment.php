<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'content',
        'email',
        'user_id',
        'video_id'
    ];
}
