<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'rating',
        'email',
        'user_id',
        'video_id',
    ];

//     public function rating() 
//     {
//         return $this->belongsTo(Video::class);
//     }
}
