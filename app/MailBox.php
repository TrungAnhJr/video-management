<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailBox extends Model
{
    protected $table = 'mail_boxes';
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'title',
        'content',
        'user_id'
    ];
}
