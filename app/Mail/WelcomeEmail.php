<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $queues;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($queues)
    {
        $this->queues = $queues;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('adminite.mail.mail_test')->with(["mails" => $this->queues]);
    }
}
