<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';
    protected $primarykey = 'id';
    // public $incrementing = false;
    protected $fillable = [
        'id',
        'name',
        'email',
        'folder_id',
        'user_id'
    ];
}
