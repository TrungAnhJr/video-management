<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = 'folders';
    protected $primarykey = 'id';
    public $incrementing = false;
    protected $fillable = [
        'name',
        'email',
        'user_id'
    ];
}
