<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'abcd',
            'email' => 'abcd1234@gmail.com',
            'email_verified_at' => null,
            'password' => bcrypt('abcd1234'),
            'remember_token' => null,
            'updated_at' => null,
            'created_at' => null,
            'permission' => 'admin'
        ]);
    }
}
