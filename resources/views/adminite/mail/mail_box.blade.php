@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">Mail Box</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12"> 
          <div class="row">
            @if(Auth::user()->permission == "admin")
            <div class ="col-md-12 text-right">
              <a href="{{ route('createMail') }}" class="btn btn-primary"><i class="fa fa-user-plus"></i>Create Mail</a>
            </div>
            @endif
          </div> 
          <div class="card">
            <div class="card-body">
              <table class="table" id="folderTable">
                <thead>
                  <tr>
                    <th>title</th>
                    <th>actions</th> 
                  </tr>
                </thead>
                <tbody id="folderTbody">
                  @foreach($mails as $mail)
                    <tr>
                      <td>
                        <a href="{{ route('showContentMail', $mail->id) }}">{{ $mail->title }}</div>
                      </td>
                      <td>
                        <a type="submit" href="{{ route('deleteMail', $mail->id) }}" class="btn btn-danger btn-sm remove" name="remove"><i class="fa fa-trash"></i></a>
                      </td>  
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $mails->links() }}
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
  
@endsection
