@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Mail Box</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-6">  
            <div class="row">
              <div class ="col-md-3">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <form action="{{ route('storeMail') }}" method="post">
               
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control"> 
                            <label for="content">Content</label>
                            <input type="text" name="content" class="form-control"> 
                          </div>
                      </div>
                      <div class="col-md-5" style="margin-left:43px">
                          <div class="form-group" >
                            <label for="userList" >User List</label><br>
                              @foreach($mails as $mail)
                                <input type="checkbox" name="userName[]" value="{{ $mail->email }}"> {{ $mail->name }}<br>
                                <input type='hidden' name='user_email' value='{{ $mail->email }}'>
                              @endforeach
                          </div>
                      </div>
                  </div>
                  <button class="btn btn-success">Send Mail</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
  
@endsection