@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <a href="{{ route('showMailBox') }}" class="btn btn-success">< Back</a>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">Mail Box</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">  
          <div class="card">
            <div class="card-body">
              <table class="table" id="folderTable">
                <thead>
                  <tr>
                    <th>content</th>
                  </tr>
                </thead>
                <tbody id="folderTbody">
                    <tr>
                      <td>
                      
                        <tr>
                          <td>
                            <div>{{ $mails['content'] }}</div>
                          </td>  
                        </tr>
                      
                      </td>  
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
  
@endsection
