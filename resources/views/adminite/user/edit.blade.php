@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <!-- <h1 class="m-0 text-dark">User Management</h1> -->
          <a href="{{ route('showUser') }}" class="btn btn-success">< Back</a>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">User Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-6">  
            <!-- <div class="row">
              <div class ="col-md-3">
                <h5>Edit User</h5>
              </div>
            </div> -->
            <div class="card">
              <div class="card-body">
                <form class="updateForm" action="{{ route('updateUser', $user->id) }}" method="post">
                  @csrf
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" value="{{ $user->name }}" name="name" class="form-control updateName">
                      </div>
                      <div class="form-group">
                          <label for="name">Email</label>
                          <input type="text" value="{{ $user->email }}" name="email" class="form-control updateEmail">
                      </div>
                    </div>
                  </div>
                  <br>
                  <button type="submit" class="btn btn-success">Edit User</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });

  $(".updateForm").submit(function(e) {
    e.preventDefault();
    
    var url = $(this).attr('action');
    let userName = $(".updateName").val();
    let userEmail = $(".updateEmail").val();
    
    $.ajax({
      url: url,
      type: "post",
      data: 
      {
        userName: userName,
        userEmail: userEmail,
      },
      success:function(response) 
      {
        var html = '';
        alert('Bạn đã sửa thông tin user thành công');
        $(".updateForm").append(html);
        window.location.reload()
      }
    })
  })
</script>
  
@endsection
