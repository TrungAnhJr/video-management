@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">User Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-12">  
            <div class="row">
              <div class ="col-md-3">
                <!-- <h5>User List</h5> -->
              </div>
              <div class ="col-md-12 text-right">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>name</th>
                      <th>email</th>
                      <th>created_at</th>
                      <th>actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>
                          <a href="{{ route('editUser', $user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                          <button type="submit" data-url="{{ route('deleteUser', $user->id) }}" class="btn btn-danger btn-sm remove" name="remove"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div>
                  {{ $users->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });

  $(".remove").click(function(e) {
    e.preventDefault();

    var url = $(this).attr('data-url');

    if(confirm('Bạn có chắc chắn muốn xóa user này không ?')) {
      $.ajax({
        url: url,
        type: "get",
        success:function(response) 
        {
          alert('Bạn đã xóa user thành công');
          window.location.reload()
        },
      }) 
    }
  })
</script>
  
@endsection
