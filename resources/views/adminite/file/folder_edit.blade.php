@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <a href="{{ route('fileManagementShowFolder') }}" class="btn btn-success">< Back</a>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">File Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-6">  
            <div class="row">
              <div class ="col-md-3">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <form class="updateForm" action="{{ route('fileManagementUpdateFolder', $folder->id) }}" method="post">
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" value="{{ $folder->name }}" name="name" class="form-control updateName">
                        </div>  
                      </div>
                  </div>
                  <br>
                  <button type="submit" class="btn btn-success">Edit Folder</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });

  $(".updateForm").submit(function(e) {
    e.preventDefault();
    
    var url = $(this).attr('action');
    let folderName = $(".updateName").val();
    
    $.ajax({
      url: url,
      type: "post",
      data: 
      {
        name:folderName,
      },
      success:function(response) 
      {
        var html = '';
        alert('Bạn đã sửa tên thư mục thành công');
        $(".updateForm").append(html);
        window.location.reload()
      }
    })
  })
</script>
  
@endsection
