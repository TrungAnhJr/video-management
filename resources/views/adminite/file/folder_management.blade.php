@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">File Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">  
          <div class="row">
            <div class ="col-md-12">
              <h6 style="color: red; opacity: 0.5"><i># Lưu ý : Nếu chưa có folder, vui lòng chờ admin tạo folder để có thể upload files</i></h6>
            </div>
            @if(Auth::user()->permission == "admin")
            <div class ="col-md-12 text-right">
              <a href="{{ route('fileManagementCreateFolder') }}" class="btn btn-primary"><i class="fa fa-user-plus"></i>Create New Folder</a>
            </div>
            @endif
          </div>
          <div class="card">
            <div class="card-body">
              <table class="table" id="folderTable">
                <thead>
                  <tr>
                    <th>name</th>
                    <th>user_name</th>
                    <th>created_at</th>
                    @if(Auth::user()->permission == "admin")
                      <th>actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody id="folderTbody">
                  @foreach($folders as $folder)
                    <tr>
                      <td><a href="{{ route('fileManagementShowVideo', $folder->id) }}">{{ $folder->name }}</a></td>
                      <td>{{ $folder->user_name }}</td>
                      <td>{{ $folder->created_at }}</td>
                      @if(Auth::user()->permission == "admin")
                        <td>
                          <a href="{{ route('fileManagementEditFolder', $folder->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                          <button type="submit" data-url="{{ route('fileManagementDeleteFolder', $folder->id) }}" class="btn btn-danger btn-sm remove" name="remove"><i class="fa fa-trash"></i></button>
                        </td>
                      @endif
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $folders->links() }}
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });

  $(".remove").click(function(e) {
    e.preventDefault();

    var url = $(this).attr('data-url');

    if(confirm('Bạn có chắc chắn muốn xóa folder này không ?')) {
      $.ajax({
        url: url,
        type: "get",
        success:function(response) 
        {
          alert('Bạn đã xóa folder thành công');
          window.location.reload()
        },
      }) 
    }
  });

</script>
  
@endsection
