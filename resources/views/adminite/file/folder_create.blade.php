@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">File Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-6">  
            <div class="row">
              <div class ="col-md-3">
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <form action="{{ route('fileManagementStoreFolder') }}" method="post">
                  @csrf
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" class="form-control">
                          </div>
                      </div>
                  </div>
                  <button class="btn btn-success">Create Folder</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
  
@endsection
