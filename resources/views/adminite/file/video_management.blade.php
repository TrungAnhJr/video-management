@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <a href="{{ route('fileManagementShowFolder') }}" class="btn btn-success">< Back</a>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">File Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">  
          <div class="row">
            <!-- @if(Auth::user()->permission == "admin")
              <div class ="col-md-12">
                <h6 style="color: red; opacity: 0.5"><i># Lưu ý : Để move video, vui lòng tạo thêm folder </i></h6>
              </div>
            @endif -->
            <div class ="col-md-12 text-right"></div>
            <div class="card">
              <div class="card-body">
                <form id="videoForm" action="{{ route('fileManagementStoreVideo', $id) }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <input id="videoFile" type="file" name="video">
                  <input type='hidden' name='folder_id' value='{{ $id }}'>
                  <button type="submit" class="btn btn-success">Upload Video</button>
                </form>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>thumbnail</th>
                    <th>name</th>
                    <th>user_name</th>
                    <th>created_at</th>
                    <th>rating</th>
                    <th>actions</th>
                  </tr>
                </thead>
                <tbody id="videoTbody">

                  @foreach($videos as $video)
                    <tr>
                      <td>
                        <img src="{{ asset($video->thumbnail_name) }}" alt="{{ $video->thumbnail_name }}" width="130" height="100">
                      </td>
                      <td>{{ $video->name }}</td>
                      <td>{{ $video->user_name }}</td>
                      <td>{{ $video->created_at }}</td>
                      <td>{{ number_format($video->avg_rating, 1) }} / 5 <span><b>☆</b></span></td>
                      <td>
                        <a href="{{ route('fileManagementViewVideoManagement', $video->id) }}" class="btn btn-primary">View</a>
                        @if(Auth::user()->permission == "admin")
                          @if($video->status != "published") 
                            <button type="submit" data-url="{{ route('fileManagementUpdateStatusVideo', $video->id) }}" class="btn btn-success publish" name="publish">Publish</button>
                          @endif
                          <button  class="btn btn-info move" name="move">Move</button>
                        @endif
                        <button type="submit" data-url="{{ route('fileManagementDeleteVideo', $video->id) }}" class="btn btn-danger remove" name="remove"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    
                  @endforeach
                </tbody>
              </table>
              <div>
                {{ $videos->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<div class="popupMove" style="cursor:pointer; display:none; height:100%; position:fixed; text-align:center; top:30%; width:100%">
  <div style="background-color: #fff; box-shadow: 10px 10px 60px #555; display: inline-block; height: auto; max-width: 551px; min-height: 100px; vertical-align: middle; width: 60%; position: relative; border-radius: 8px; padding: 15px 5% ">
    <label for="folderList">Folder List</label>
    <hr>
    @foreach($folders as $folder)
    <div style="text-align:left;">
      <input id="popupChooseFolder" type="radio" name="popupChooseFolder" value="{{ $folder->id }}"> {{ $folder->name }}<br>
    </div>
    @endforeach
    <br>
    <button type="button" data-url="{{ route('fileManagementMoveVideo', $id) }}" id="popupSubmitFolder" name="popupSubmitFolder" class="btn btn-success">Choose Folder</button>
  </div>
</div>



<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });

  $(".move").click(function(e) {
    e.preventDefault();
    $(".popupMove").show(); 
  
    $("#popupSubmitFolder").click(function(e) { 
      e.preventDefault();
      $(".popupMove").hide();

      var url = $(this).attr('data-url');
      let popupChooseFolder = $('input[name=popupChooseFolder]:checked').val();
      let popupSubmitFolder = $(this).val();

      if(popupChooseFolder === undefined) {
        alert('Bạn phải chọn folder trước');
      
      } else {
        $.ajax({
          url: url,
          type: "post",
          data: 
          {
            popupChooseFolder: popupChooseFolder,
          },
          success:function(response) 
          {
            var html = '';
            $("#popupSubmitFolder").append(html); 
            alert('Bạn đã move video thành công');
            window.location.reload()
          }
        })
      } 
    }); 
  });

  $(".publish").click(function(e) {
    e.preventDefault();

    var url = $(this).attr('data-url');
    let publish = $(this).val();

    $.ajax({
      url: url,
      type: "post",
      data: 
      {
        publish: publish,
      },
      success:function(response) 
      {
        var html = '';
        alert('Bạn đã phê duyệt video thành công');
        $(".publish").append(html);
        window.location.reload()
      },
    })    
  });

  $(".remove").click(function(e) {
    e.preventDefault();

    var url = $(this).attr('data-url');

    if(confirm('Bạn có chắc chắn muốn xóa video này không ?')) {
      $.ajax({
        url: url,
        type: "get",
        success:function(response) 
        {
          alert('Bạn đã xóa video thành công');
          window.location.reload()
        },
      }) 
    }
  });

  var MAX_FILE_SIZE = 13 * 1024 * 1024;

  $(document).ready(function() {
    $("#videoFile").change(function() {
      fileSize = this.files[0].size;
      if (fileSize > MAX_FILE_SIZE) {
        this.setCustomValidity("Dung lượng file tối đa là 13MB");
        this.reportValidity();
      } else {
        this.setCustomValidity("");
      }
    });

    $("#videoForm").submit(function(e) {
      e.preventDefault();

      var url = $(this).attr('action');
      let thumbnailImage = $(this).val();

      $.ajax({
        url: url,
        data: new FormData(this), thumbnailImage: thumbnailImage,
        type: 'post',
        contentType: false, 
        processData: false,
        success:function(response) 
        {
          var html = '<tr>';
          html += '<td>';
          html += '<img src="" alt="' + response.data.thumbnail_name + '" width="130" height="100">';
          html += '</td>';
          html += '<td>' + response.data.name + '</td>';
          html += '<td>' + response.data.user_name + '</td>';
          html += '<td>' + response.data.created_at + '</td>';
          html += '<td>' + '0.0 / 5 <span><b>☆</b></span>' + '</td>';
          html += '<td>';
          html += '<a href="https://video-management.herokuapp.com/file-management/viewVideo/' + response.data.id + '" class="btn btn-primary"> View </a>';
          html += '@if(Auth::user()->permission == "admin") <button type="submit" data-url="https://video-management.herokuapp.com/file-management/updateStatusVideo/' + response.data.id + '" class="btn btn-success publish" name="publish"> Publish </button>';
          html += ' <button type="submit" data-url="https://video-management.herokuapp.com/file-management/deleteVideo/' + response.data.id + '" class="btn btn-danger remove" name="remove"><i class="fa fa-trash"></i></button>'; 
          html += ' <button type="submit" data-url="https://video-management.herokuapp.com/file-management/moveVideo/' + response.data.id + '" class="btn btn-info move" name="move">Move</button> @endif';
          html += '</td>';
          html += '</tr>';

          $("#videoTbody").prepend(html);
          alert('Bạn đã upload file thành công. Vui lòng chờ Admin phê duyệt');
          window.location.reload();

          $(".publish").click(function(e) {
            e.preventDefault();

            var url = $(this).attr('data-url');
            let publish = $(this).val();

            $.ajax({
              url: url,
              type: "post",
              data: 
              {
                publish: publish,
              },
              success:function(response) 
              {
                var html = '';
                alert('Bạn đã phê duyệt video thành công');
                
                $(".publish").append(html);
                window.location.reload()
              },
            })    
          });

          $(".remove").click(function(e) {
            e.preventDefault();

            var url = $(this).attr('data-url');

            if(confirm('Bạn có chắc chắn muốn xóa video này không ?')) {
              $.ajax({
                url: url,
                type: "get",
                success:function(response) 
                {
                  alert('Bạn đã xóa video thành công');
                  window.location.reload()
                },
              }) 
            }
          });
        }
      })
    })
  });

</script>


  
@endsection
