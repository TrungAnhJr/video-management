@extends('adminite.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <a href="{{ route('fileManagementShowVideo', $video->folder_id) }}" class="btn btn-success">< Back</a>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><span>Home</span></li>
            <li class="breadcrumb-item active">File Management</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
        <div class="row justify-content-center">
          <div class="col-md-6">  
            <iframe src="{{ asset('video/'.$folderInfo->name.'/'.$video->name) }}" style="width:800px; height:500px"></iframe>
          </div>
        </div>
        <hr>
        <div class="row justify-content-center">
          <div class="col-md-3 text-left"> 
            <div class="rating">
              <span>Đánh giá video</span>
              <form id="ratingForm" action="{{ route('fileManagementStoreRating', $video->id) }}" method="post">
              @csrf
                <div class="rating-display">
                  @for($i = 5; $i >= 1; $i--)  
                    <span id="ratingSubmit" type="submit" class="get-rating" name="rate" data-value="{{ $i }}">☆</span>
                    <input type='hidden' name='video_id' value='{{ $id }}'>
                  @endfor
                </div>
              </form>
            </div> 
          </div>
          <div class="col-md-3 text-right">
            <div>
              <form action="{{ route('fileManagementDownloadZipVideo', $video->id) }}" method="post">
              @csrf
                <button class="btn btn-success">Download</button>
              </form>
            </div>
          </div>
        </div>
        <hr>
        <div class="card">
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>email</th>
                  <th>content</th>
                  <th>created_at</th>
                </tr>
              </thead>
              <tbody id="commentTbody">
                @foreach($comments as $comment)
                  <tr>
                    <td>{{ $comment->email }}</td>
                    <td>{{ $comment->content }}</td>
                    <td>{{ $comment->created_at }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div>
              {{ $comments->links() }}
            </div>
            <form id="commentForm" action="{{ route('fileManagementStoreComment', $id) }}" method="post">
              @csrf
              <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="name">Comment</label>
                          <input type="text" name="content" class="form-control" id="commentContent">
                          <input type="hidden" name="video_id" value='{{ $id }}'>
                      </div>
                  </div>
              </div>
              <button type="submit" class="btn btn-success">Comment</button>
            </form>
          </div>
        </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });

  $("#commentForm").submit(function(e) {
    e.preventDefault();
    
    var url = $(this).attr('action');
    let content = $("#commentContent").val();
    
    $.ajax({
      url: url,
      type: "post",
      data: 
      {
        content:content,
      },
      success:function(response) 
      {
        var html = '<tr>';
        html += '<td>' + response.data.email + '</td>';
        html += '<td>' + response.data.content + '</td>';
        html += '<td>' + response.data.created_at + '</td>';
        html += '</tr>';

        $("#commentTbody").append(html);
      }
    })
  })

  $(".get-rating").click(function(e) {
    e.preventDefault();

    var url = $("#ratingForm").attr('action');
    let rating = $(this).attr('data-value');

    $.ajax({
      url: url,
      type: "post",
      data: 
      {
        rate:rating,
      },
      success:function(response) 
      {
        var html = '';

        alert('Bạn đã đánh giá ' + response.data.rating + ' trên 5 sao cho video này');
        $(".rating-display").append(html);
      }
    })
  })

  
</script>

@endsection


