<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('home', function () {
    return redirect()->route('dashboard');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['checkuser'])->group(function(){
    Route::get('user-management', 'UserController@userManagement');

    Route::get('user-management', 'UserController@showUser')->name('showUser');
    Route::get('user-management/createUser', 'UserController@createUser')->name('createUser');
    Route::post('user-management/storeUser', 'UserController@storeUser')->name('storeUser');
    Route::get('user-management/editUser/{id}', 'UserController@editUser')->name('editUser');
    Route::post('user-management/updateUser/{id}', 'UserController@updateUser')->name('updateUser');
    Route::get('user-management/deleteUser/{id}', 'UserController@deleteUser')->name('deleteUser');
});

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('fblogin');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback')->name('fbcallback');

Route::middleware(['checkip'])->group(function(){
    Route::get('file-management/folder-management', 'FolderController@showFolder')->name('fileManagementShowFolder');
});
Route::get('file-management/createFolder', 'FolderController@createFolder')->name('fileManagementCreateFolder');
Route::post('file-management/storeFolder', 'FolderController@storeFolder')->name('fileManagementStoreFolder');
Route::get('file-management/editFolder/{id}', 'FolderController@editFolder')->name('fileManagementEditFolder');
Route::post('file-management/updateFolder/{id}', 'FolderController@updateFolder')->name('fileManagementUpdateFolder');
Route::get('file-management/deleteFolder/{id}', 'FolderController@deleteFolder')->name('fileManagementDeleteFolder');

Route::get('file-management/video-management/{id}', 'FileController@showVideo')->name('fileManagementShowVideo');
Route::post('file-management/updateStatusVideo/{id}', 'FileController@updateStatusVideo')->name('fileManagementUpdateStatusVideo');
Route::get('file-management/viewVideo/{id}', 'FileController@viewVideoManagement')->name('fileManagementViewVideoManagement');
Route::get('file-management/deleteVideo/{id}', 'FileController@deleteVideo')->name('fileManagementDeleteVideo');
Route::post('file-management/moveVideo/{id}', 'FileController@moveVideo')->name('fileManagementMoveVideo');

Route::post('file-management/storeVideo/{id}', 'FileController@storeVideo')->name('fileManagementStoreVideo');
Route::post('file-management/downloadZipVideo/{id}', 'FileController@downloadZipVideo')->name('fileManagementDownloadZipVideo');
Route::post('file-management/storeComment/{id}', 'FileController@storeComment')->name('fileManagementStoreComment');
Route::post('file-management/storeRating/{id}', 'FileController@storeRating')->name('fileManagementStoreRating');

Route::get('/user/find', 'FileController@searchByName')->name('searchByName');

Route::get('mail-box', 'MailController@showMailBox')->name('showMailBox');
Route::get('mail-box/contentMail/{id}', 'MailController@showContentMail')->name('showContentMail');
Route::get('mail-box/deleteMail/{id}', 'MailController@deleteMail')->name('deleteMail');
Route::get('mail-box/createMail', 'MailController@createMail')->name('createMail');
Route::post('mail-box/storeMail', 'MailController@storeMail')->name('storeMail');

// Route::get('mail-queue', 'MailController@processQueue')->name('processQueue');

// Route::get('geoip', 'FileController@getUserIp')->name('getUserIp');

Route::get('try-again', 'FileController@tryAgain')->name('tryAgain');


















