<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\ApiHomeController@login');
    Route::post('logout', 'Api\ApiHomeController@logout');
    Route::post('refresh', 'Api\ApiHomeController@refresh');
    Route::post('me', 'Api\ApiHomeController@me');
    Route::post('folder', 'Api\ApiHomeController@index');
    Route::post('storeFolder', 'Api\ApiHomeController@storeFolder')->name('storeFolder');
    Route::post('upload-file/storeVideo/{id}', 'Api\ApiHomeController@storeVideo')->name('storeVideo');
});











